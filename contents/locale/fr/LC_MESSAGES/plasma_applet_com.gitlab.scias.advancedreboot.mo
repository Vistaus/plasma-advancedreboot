��          �      �       H  &   I     p  "   �  +   �     �     �  U   �  C   :     ~     �     �     �  �   �  5  e  6   �     �      �  /        8  	   K  [   U  g   �          9     T     h  �   w                                                          
   	    </b> has been set for the next reboot. Advanced reboot Behavior upon selecting an entry : Displayed boot entries in the plasmoid view Don't reboot General No boot entries could be found.
Please check that your system meets the requirements. No boot entries could be listed.
Please check this applet settings. Reboot after confirmation Reboot immediately Reboot into... The entry <b> This applet cannot work on this system.
Please check that the system is booted in UEFI mode and that systemd, systemd-boot are used and configured properly. Project-Id-Version: advancedreboot
Report-Msgid-Bugs-To: https://www.gitlab.com/Scias/plasma-advancedreboot
PO-Revision-Date: 2024-03-16 21:00+0100
Last-Translator: Scias <khirac@proton.me>
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 </b> a bien été définie pour le prochain démarrage Redémarrage avancé Comportement après sélection : Entrées de démarrage affichées dans l'applet Ne pas redémarrer Général Aucune entrée n'a pu être trouvée.
Merci de vérifier l'éligibilité de votre système. Aucune entrée de démarrage ne peut être affichée.
Merci de vérifier les paramètres de cet applet. Redémarrer après confirmation Redémarrer immédiatement Redémarrer vers... L'éntrée <b> Cet applet ne peut pas fonctionner sur ce système.
Please check that the system is booted in UEFI mode and that systemd, systemd-boot are used and configured properly. 